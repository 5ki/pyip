from flask import Flask
from flask import request
from flask import render_template, url_for

def create_app():
	app = Flask(__name__)

	app.config.from_pyfile('config.py')

	@app.route("/")
	def auto():
		if is_browser(request.headers.get('User-Agent')):
			return html()
		return text()

	@app.route('/html')
	def html():
		return render_template('index.html', headers=app.config['HEADERS'])

	@app.route('/html/full')
	def html_full():
		return render_template('index.html', headers=request.headers.keys())		

	@app.route('/text')
	@app.route('/txt')
	def text():
		return request.remote_addr + '\n'

	@app.route('/headers')
	def headers():
		result = '\n' + text() + '---\n'
		for hdr in app.config['HEADERS']:
			result += f'{hdr}: {request.headers.get(hdr)}\n'
		return result + '\n'

	@app.route('/headers/full')
	def headers_full():
		result = '\n' + text() + '---\n'
		for hdr in request.headers:
			result += f'{hdr[0]}: {hdr[1]}\n'
		return result + '\n'

	def is_browser(ua):
		for b in app.config['BROWSERS']:
			if b in ua:
				return True
		return False

	return app

app = create_app()
