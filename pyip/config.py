# If one of these strings matches against User-Agent, html is shown at '/', text otherwise
BROWSERS = ['Chrome', 'Gecko', 'WebKit', 'Trident', 'Opera', 'Lynx', 'w3m']

# Additional headers to display in html or full mode
HEADERS = ['Forwarded', 'X-Forwarded-For', 'X-Forwarded-Host', 'X-Forwarded-Proto', 'Via']